# The Prisoner's Dilemma: A Simulation of Choice
# Variables
count = 0
human_score = 0
computer_score = 0

human_cooperate_score = 0
computer_cooperate_score = 0
human_cheat_score = 0
computer_cheat_score = 0
human_flipper_score = 0
computer_flipper_score = 0
human_skipper_score = 0
computer_skipper_score = 0

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Start game

def StartGame():
    print("Welcome to: The Prisoner's Dilemma: A Simulation of Choice!")
    print("In this simulation you will go against different computer programs in a game")
    print("Each program is unique in how they play")
    print("Play against them to try to figure out their strategy")
    clear_one()
    print("The point system works as the following:")
    print("If you both cooperate, you gain 2 points each")
    print("If you both defect, you both lose 1 point each")
    print("If one of you defects and the other cooperates, the defector will gain 3 points while the cooperator loses 1")
    print("Learn each one's strategy and gain as many points as possible")
    print("Good luck and have fun!")
    print("Starting experiment...")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Clear Functions
def clear_one():
    print("                                                          ")
    print("----------------------------------------------------------")
    print("                                                          ")

def clear_double():
    print("                                                          ")
    print("==========================================================")
    print("                                                          ")

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Score Changers
# Cooperate vs Cooperate: +2 +2
# Defect vs Defect: -1 -1
# Cooperate vs Defect: -1 +3

def add_human_score(h):
    global human_score
    if h == 3:
        human_score += h
    elif h == 2:
        human_score += h
    elif h == 1:
        human_score = human_score - h
    return human_score

def add_computer_score(c):
    global computer_score
    if c == 3:
        computer_score = computer_score + c
    elif c == 2:
        computer_score = computer_score + c
    elif c == 1:
        computer_score = computer_score - c
    return computer_score

def counter(c):
    global count
    count = count + c
    return count

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Ends game once all computer strategies are played

def completer():
    if "1" and "2" and "3" and "4" in done_list:
        clear_one()
        print("You have played against all strategies")
        print("Your scores for each strategies is as such")
        print("Strategy 1: Your score ", human_cooperate_score, "Opponent's score", computer_cooperate_score)
        print("Strategy 2: Your score ", human_cheat_score, "Opponent's score", computer_cheat_score)
        print("Strategy 3: Your score ", human_flipper_score, "Opponent's score", computer_flipper_score)
        print("Strategy 4: Your score ", human_skipper_score, "Opponent's score", computer_skipper_score)
        print("Play again to get different or higher scores!")
        exit()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Prompts user to chose what computer strategy to go against

def chooser():
    global human_score
    global computer_score
    global count
    human_score = 0
    computer_score = 0
    count = 0
    clear_double()
    print("You have played stategy(s): ", done_list)
    completer()
    pick = input("Enter a number 1-4 to try a strategy ")

    if pick == "1":
        print("You chose to play against the first strategy")
        clear_double()
        cooperater()

    elif pick == "2":
        print("You chose to play against the second strategy")
        clear_double()
        cheater()

    elif pick == "3":
        print("You chose to play against the third strategy")
        clear_double()
        flipper()

    elif pick == "4":
        print("You chose to play against the fourth strategy")
        clear_double()
        skipper()

    else:
        print("That's not an option")
        chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Computer always cooperates
# Max possible score: 33
def cooperater():
    global human_score
    global computer_score
    global count
    global human_cooperate_score
    global computer_cooperate_score
    human_score = 0
    computer_score = 0
    count = 0
    while count < 11:
        clear_one()
        print("This is round", count, ":")
        print("Your current score is ", human_score)
        print("Your opponent's score is ", computer_score)

        pick = input("Enter 'cooperate' or 'defect' ")
        if pick == "cooperate":
            add_human_score(2)
            add_computer_score(2)
            counter(1)

        elif pick == "defect":
            add_human_score(3)
            add_computer_score(1)
            counter(1)

        else:
            print("That's not an option")

    human_cooperate_score = human_score
    computer_cooperate_score = computer_score
    done_list.append("1")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Computer always defects
# Max possible score: -11
def cheater():
    global human_score
    global computer_score
    global count
    global human_cheat_score
    global computer_cheat_score
    human_score = 0
    computer_score = 0
    count = 0
    while count < 11:
        clear_one()
        print("This is round", count, ":")
        print("Your current score is ", human_score)
        print("Your opponent's score is ", computer_score)

        pick = input("Enter 'cooperate' or 'defect' ")
        if pick == "cooperate":
            human_score = add_human_score(1)
            computer_score = add_computer_score(3)
            counter(1)

        elif pick == "defect":
            human_score = add_human_score(1)
            computer_score = add_computer_score(1)
            counter(1)

        else:
            print("That's not an option")


    human_cheat_score = human_score
    computer_cheat_score = computer_score
    done_list.append("2")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Computer flips between cooperating and defecting
# Max possible score: 13
def flipper():
    global human_score
    global computer_score
    global count
    global human_flipper_score
    global computer_flipper_score
    human_score = 0
    computer_score = 0
    count = 0
    while count < 11:
        clear_one()
        print("This is round", count, ":")
        print("Your current score is ", human_score)
        print("Your opponent's score is ", computer_score)
        pick = input("Enter 'cooperate' or 'defect' ")
        # Cooperates every even number for count
        if count % 2 == 0:
            if pick == "cooperate":
                add_human_score(2) # +2
                add_computer_score(2) # +2
                counter(1)

            elif pick == "defect":
                add_human_score(3) # +3
                add_computer_score(1) # -1
                counter(1)
        # Defects every odd number for count
        elif count % 2 != 0:
            if pick == "cooperate":
                human_score = add_human_score(1) # -1
                computer_score = add_computer_score(3) # +3
                counter(1)
            elif pick == "defect":
                human_score = add_human_score(1) # -1
                computer_score = add_computer_score(1) # -1
                counter(1)

    human_flipper_score = human_score
    computer_flipper_score = computer_score
    done_list.append("3")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
# Computer cooperates always unless the round is a multipe of 3
# Max possible score: 17
def skipper():
    global human_score
    global computer_score
    global count
    global human_skipper_score
    global computer_skipper_score
    human_score = 0
    computer_score = 0
    count = 0
    while count < 11:
        clear_one()
        print("This is round", count, ":")
        print("Your current score is ", human_score)
        print("Your opponent's score is ", computer_score)
        pick = input("Enter 'cooperate' or 'defect' ")
        # Cooperates every round unless the round is a multipe of 3
        if count % 3 != 0:
            if pick == "cooperate":
                add_human_score(2) # +2
                add_computer_score(2) # +2
                counter(1)
            elif pick == "defect":
                add_human_score(3) # +3
                add_computer_score(1) # -1
                counter(1)
        # If the round is a multiple of 3, it defects
        elif count % 3 == 0:
            if pick == "cooperate":
                human_score = add_human_score(1) # -1
                computer_score = add_computer_score(3) # +3
                counter(1)
            elif pick == "defect":
                human_score = add_human_score(1) # -1
                computer_score = add_computer_score(1) # -1
                counter(1)

    human_skipper_score = human_score
    computer_skipper_score = computer_score
    done_list.append("4")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

done_list = []
StartGame()