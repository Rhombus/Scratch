# The Prisoner's Dilemma: A Simulation of Choice

print("Welcome to the game!")
print("This game will have different computer programs to play against")

def StartGame():
    print("Starting thought experiment...")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

def clear1():
    print("                                                          ")
    print("==========================================================")
    print("                                                          ")

def clear2():
    print("                                                          ")
    print("----------------------------------------------------------")
    print("                                                          ")

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

def h_scorer(h):
    if h > 0:
        h_score = h_score + h
    elif h < 0:
        h_score = h_score + h

def c_scorer(c):
    if c > 0:
        c_score = c_score + c
    elif c < 0:
        c_score = c_score + c

def counter(c):
    #count = count + c
    return count + c

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

def completer():
    if "1" in done_list:
        clear()
        print("You have played against all strategies")
        print("Your scores for each strategies is as such")
        print("Strategy 1: Your score ", cooperate_score, "Opponent's score", a_cooperate_score)
        print("Strategy 2: Your score ", cheat_score, "Opponent's score", a_cheat_score)
        print("Strategy 3: Your score ", copycat_score, "Opponent's score", a_copycat_score)
        print("Strategy 4: Your score ", skipper_score, "Opponent's score", a_skipper_score)
        print("Strategy 5: Your score ", analyzer_score, "Opponent's score", a_analyzer_score)
        exit()

#and "2" and "3" and "4" and "5"
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

def chooser():
    count = 0
    h_score = 0
    c_score = 0
    clear1()
    print("You have done stategy(s): ", done_list)
    completer()
    pick = input("Enter a number 1-5 to try each strategy ")

    if pick == "1":
        print("You chose to play against the first strategy")
        clear1()
        cooperater()

    elif pick == "2":
        print("You chose to play against the second strategy")
        clear1()
        cheater()

    elif pick == "3":
        print("You chose to play against the third strategy")
        clear1()
        copycat()

    elif pick == "4":
        print("You chose to play against the fourth strategy")
        clear1()
        skipper()

    elif pick == "5":
        print("You chose to play against the fifth strategy")
        clear1()
        analyzer()

    else:
        print("That's not an option")
        chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

def cooperater():
    count = 0
    h_score = 0
    c_score = 0
    while count < 11:
        clear2()
        print("This is round", count, ":")
        print("Your current score is ", h_score)
        print("Your opponent's score is ", c_score)

        choice = input("Enter 'cooperate' or 'defect' ")
        if choice == "cooperate":
            h_score = h_scorer(+2)
            c_score = c_scorer(+2)
            counter(1)

        elif choice == "defect":
            h_score = h_scorer(+3)
            c_score = c_scorer(-1)
            counter(1)

        else:
            print("That's not an option")

    cooperate_score = h_score
    a_cooperate_score = c_score
    done_list.append("1")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

def cheater():
    count = 0
    h_score = 0
    C_score = 0
    while count < 11:
        clear2()
        print("This is round", count, ":")
        print("Your current score is ", h_score)
        print("Your opponent's score is ", c_score)

        choice = input("Enter 'cooperate' or 'defect' ")
        lines()
        if choice == "cooperate":
            h_score = h_scorer(-1)
            c_score = c_scorer(+3)
            counter(1)

        elif choice == "defect":
            h_score = h_scorer(-2)
            c_score = c_scorer(-2)
            counter(1)

        else:
            print("That's not an option")


    cheat_score = h_score
    a_cheat_score = c_score
    done_list.append("2")
    chooser()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#





#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

done_list = []
StartGame()